import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './components/dashboard/start/start.component';
import { LogsComponent } from './components/dashboard/logs/logs.component';
import { DefectsComponent } from './components/dashboard/defects/defects.component';

const routes: Routes = [
  {
    path: 'start',
    component: StartComponent,
  },
  {
    path: 'components',
    loadChildren: () => import('./components/dashboard/components/components.module')
      .then(m => m.ComponentsModule)
  },
  {
    path: 'errors',
    loadChildren: () => import('./components/dashboard/errors/errors.module')
      .then(m => m.ErrorsModule)
  },
  {
    path: 'metrics',
    loadChildren: () => import('./components/dashboard/metrics/metrics.module')
      .then(m => m.MetricsModule)
  },
  {
    path: 'checks',
    loadChildren: () => import('./components/dashboard/checks/checks.module')
      .then(m => m.ChecksModule)
  },
  {
    path: 'logs',
    component: LogsComponent,
  },
  {
    path: 'defects',
    component: DefectsComponent,
  },
  {
    path: 'settings',
    loadChildren: () => import('./components/dashboard/settings/settings.module')
      .then(m => m.SettingsModule)
  },
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
