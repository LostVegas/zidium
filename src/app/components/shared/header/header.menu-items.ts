export const MENU_ITEMS = [
  {
    name: 'Старт',
    link: '/start',
    active: false
  },
  {
    name: 'Компоненты',
    show: false,
    link: '/components/component-tree',
    active: false,
    children: [
      {
        name: 'Список',
        link: '/components/list'
      },
      {
        name: 'Текущее состояние',
        link: '/components/current-state'
      },
      {
        name: 'История',
        link: '/components/history'
      },
      {
        name: 'Проблемные компоненты',
        link: '/components/problem-components'
      },
      {
        name: 'Типы компонентов',
        link: '/components/types'
      },
    ]
  },
  {
    name: 'Ошибки',
    show: false,
    link: '/errors/errors',
    active: false,
    children: [
      {
        name: 'События',
        link: '/errors/events'
      },
      {
        name: 'Типы событий',
        link: '/errors/event-types'
      },
    ]
  },
  {
    name: 'Метрики',
    show: false,
    link: '/metrics/metrics',
    active: false,
    children: [
      {
        name: 'Архив значений метрик',
        link: '/metrics/metrics-data'
      },
      {
        name: 'Типы метрик',
        link: '/metrics/metric-types'
      },
    ]
  },
  {
    name: 'Проверки',
    show: false,
    link: '/checks/checks-results',
    active: false,
    children: [
      {
        name: 'Список',
        link: '/checks/check-list'
      },
      {
        name: 'Типы проверок',
        link: '/checks/check-types'
      },
    ]
  },
  {
    name: 'Лог',
    show: false,
    link: '/logs',
    active: false,
  },
  {
    name: 'Дефекты',
    show: false,
    link: '/defects',
    active: false,
  },
  {
    name: '',
    show: false,
    link: '/settings',
    active: false,
    children: [
      {
        name: 'Пользователи',
        link: '/settings/users'
      },
      {
        name: 'Подписки',
        link: '/settings/subscriptions'
      },
      {
        name: 'Уведомления',
        link: '/settings/notifications'
      },
      {
        name: 'Аккаунт',
        link: '/settings/account-info'
      },
      {
        name: 'Лимиты',
        link: '/settings/limits'
      },
      {
        name: 'Тариф',
        link: '/settings/tariff'
      }
    ]
  }
];
