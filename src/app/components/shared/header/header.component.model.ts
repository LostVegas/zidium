export class HeaderComponentModel {
  email: string;
  isShowMenu: boolean;
  isUser: boolean;
  isShowAccount: boolean;
}
