import { Component, OnInit } from '@angular/core';
import { HeaderComponentModel } from './header.component.model';
import { MENU_ITEMS } from './header.menu-items';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  windowWidth: number = window.innerWidth;
  model: HeaderComponentModel;
  menuItems = MENU_ITEMS;

  constructor(private router: Router) {
    this.model = new HeaderComponentModel();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.checkParentUrl(this.router.url);
      }
    });
  }

  ngOnInit(): void {
    this.model.isUser = true;
    this.model.email = 'demo@zidium.net';
    if (this.windowWidth > 800) {
      this.model.isShowMenu = true;
    }
  }

  toggleMenu(): void {
    this.model.isShowMenu = !this.model.isShowMenu;
  }

  toggleMenuItem(item): void {
    item.show = !item.show;
    this.menuItems.map(el => {
      if (item.name !== el.name) {
        el.show = false;
      }
    });
  }

  onClickedOutside(): void {
    this.menuItems.map(el => {
      el.show = false;
    });
  }

  onClickedOutsideAccount(): void {
    if (this.model.isShowAccount) {
      this.model.isShowAccount = false;
    }
  }

  toggleAccount(): void {
    this.model.isShowAccount = !this.model.isShowAccount;
  }

  onCloseDropdownMenu(): void {
    this.menuItems.map(el => {
      el.show = false;
    });
  }

  checkParentUrl(currentUrl): void {
    this.menuItems.forEach(item => {
      let links = [item.link];
      if (item.children) {
        links.push(...item.children.map(el => el.link));
      }
      item.active = links.includes(currentUrl);
    });
  }

  logOut() {
    return null;
  }

}
