import { Component, OnInit } from '@angular/core';
import { SampleComponentModel } from './sample.component.model';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {

  constructor() {
    this.model = new SampleComponentModel();
  }

  /**
   * Модель данных
   */
  model: SampleComponentModel;

  ngOnInit(): void {
    this.model.text = 'Текст';
  }

  onButtonClick(): void {
    this.model.text = 'Новый текст';
  }

}
