import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsLayoutComponent } from './settings-layout/settings-layout.component';
import { UsersComponent } from './users/users.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { LimitsComponent } from './limits/limits.component';
import { TariffComponent } from './tariff/tariff.component';



const routes: Routes = [
  {
    path: '',
    component:  SettingsLayoutComponent,
    children: [
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'subscriptions',
        component: SubscriptionsComponent,
      },
      {
        path: 'notifications',
        component: NotificationsComponent,
      },
      {
        path: 'account-info',
        component: AccountInfoComponent,
      },
      {
        path: 'limits',
        component: LimitsComponent,
      },
      {
        path: 'tariff',
        component: TariffComponent,
      },
      {
        path: '',
        redirectTo: '/',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
