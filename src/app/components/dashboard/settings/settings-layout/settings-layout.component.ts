import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings-layout',
  template: `<router-outlet></router-outlet>`,
})
export class SettingsLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
