import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationsComponent } from './notifications/notifications.component';
import { AccountInfoComponent } from './account-info/account-info.component';
import { LimitsComponent } from './limits/limits.component';
import { TariffComponent } from './tariff/tariff.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { UsersComponent } from './users/users.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsLayoutComponent } from './settings-layout/settings-layout.component';

@NgModule({
  declarations: [
    NotificationsComponent,
    AccountInfoComponent,
    LimitsComponent,
    TariffComponent,
    SubscriptionsComponent,
    UsersComponent,
    SettingsLayoutComponent,
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
  ],
  providers: []
})
export class SettingsModule { }
