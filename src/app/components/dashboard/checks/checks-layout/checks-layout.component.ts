import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checks-layout',
  template: `<router-outlet></router-outlet>`,
})
export class ChecksLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
