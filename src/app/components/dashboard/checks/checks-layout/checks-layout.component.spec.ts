import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecksLayoutComponent } from './checks-layout.component';

describe('ChecksLayoutComponent', () => {
  let component: ChecksLayoutComponent;
  let fixture: ComponentFixture<ChecksLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecksLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecksLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
