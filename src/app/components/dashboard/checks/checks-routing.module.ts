import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ChecksLayoutComponent} from './checks-layout/checks-layout.component';
import {ChecksResultsComponent} from './checks-results/checks-results.component';
import {CheckListComponent} from './check-list/check-list.component';
import {CheckTypesComponent} from './check-types/check-types.component';



const routes: Routes = [
  {
    path: '',
    component:  ChecksLayoutComponent,
    children: [
      {
        path: 'checks-results',
        component: ChecksResultsComponent,
      },
      {
        path: 'check-list',
        component: CheckListComponent,
      },
      {
        path: 'check-types',
        component: CheckTypesComponent,
      },
      {
        path: '',
        redirectTo: 'metrics',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChecksRoutingModule { }
