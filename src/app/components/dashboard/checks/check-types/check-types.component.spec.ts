import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckTypesComponent } from './check-types.component';

describe('CheckTypesComponent', () => {
  let component: CheckTypesComponent;
  let fixture: ComponentFixture<CheckTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
