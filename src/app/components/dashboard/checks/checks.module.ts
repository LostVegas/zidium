import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChecksLayoutComponent } from './checks-layout/checks-layout.component';
import { ChecksResultsComponent } from './checks-results/checks-results.component';
import { CheckListComponent } from './check-list/check-list.component';
import { CheckTypesComponent } from './check-types/check-types.component';
import { ChecksRoutingModule } from './checks-routing.module';

@NgModule({
  declarations: [
    ChecksLayoutComponent,
    ChecksResultsComponent,
    CheckListComponent,
    CheckTypesComponent,
  ],
  imports: [
    CommonModule,
    ChecksRoutingModule,
  ],
  providers: []
})
export class ChecksModule { }
