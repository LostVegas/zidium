import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecksResultsComponent } from './checks-results.component';

describe('ChecksResultsComponent', () => {
  let component: ChecksResultsComponent;
  let fixture: ComponentFixture<ChecksResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecksResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecksResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
