import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MetricsComponent } from './metrics/metrics.component';
import { MetricsLayoutComponent } from './metrics-layout/metrics-layout.component';
import { MetricsDataComponent } from './metrics-data/metrics-data.component';
import { MetricTypesComponent } from './metric-types/metric-types.component';
import { MetricsRoutingModule } from './metrics-routing.module';

@NgModule({
  declarations: [
    MetricsComponent,
    MetricsLayoutComponent,
    MetricsDataComponent,
    MetricTypesComponent,
  ],
  imports: [
    CommonModule,
    MetricsRoutingModule,
  ],
  providers: []
})
export class MetricsModule { }
