import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricTypesComponent } from './metric-types.component';

describe('MetricTypesComponent', () => {
  let component: MetricTypesComponent;
  let fixture: ComponentFixture<MetricTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
