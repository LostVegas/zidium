import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-metrics-layout',
  template: `<router-outlet></router-outlet>`,
})
export class MetricsLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
