import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MetricsDataComponent} from './metrics-data/metrics-data.component';
import {MetricTypesComponent} from './metric-types/metric-types.component';
import {MetricsComponent} from './metrics/metrics.component';
import {MetricsLayoutComponent} from './metrics-layout/metrics-layout.component';



const routes: Routes = [
  {
    path: '',
    component:  MetricsLayoutComponent,
    children: [
      {
        path: 'metrics',
        component: MetricsComponent,
      },
      {
        path: 'metrics-data',
        component: MetricsDataComponent,
      },
      {
        path: 'metric-types',
        component: MetricTypesComponent,
      },
      {
        path: '',
        redirectTo: 'metrics',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MetricsRoutingModule { }
