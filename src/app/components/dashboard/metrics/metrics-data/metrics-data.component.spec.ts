import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsDataComponent } from './metrics-data.component';

describe('MetricsDataComponent', () => {
  let component: MetricsDataComponent;
  let fixture: ComponentFixture<MetricsDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
