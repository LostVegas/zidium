import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsTypesComponent } from './components-types.component';

describe('ComponentsTypesComponent', () => {
  let component: ComponentsTypesComponent;
  let fixture: ComponentFixture<ComponentsTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
