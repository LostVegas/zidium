import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components-layout',
  template: `<router-outlet></router-outlet>`,
})
export class ComponentsLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
