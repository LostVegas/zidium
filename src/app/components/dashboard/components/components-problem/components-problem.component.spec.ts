import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsProblemComponent } from './components-problem.component';

describe('ComponentsProblemComponent', () => {
  let component: ComponentsProblemComponent;
  let fixture: ComponentFixture<ComponentsProblemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsProblemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsProblemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
