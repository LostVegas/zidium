import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsTree } from './components-tree.component';

describe('ComponentsComponent', () => {
  let component: ComponentsTree;
  let fixture: ComponentFixture<ComponentsTree>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsTree ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsTree);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
