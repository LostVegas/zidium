import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsHistoryComponent } from './components-history.component';

describe('ComponentsHistoryComponent', () => {
  let component: ComponentsHistoryComponent;
  let fixture: ComponentFixture<ComponentsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
