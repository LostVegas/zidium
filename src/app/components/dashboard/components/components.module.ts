import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsTree } from './components-tree/components-tree.component';
import { ComponentsListComponent } from './components-list/components-list.component';
import { ComponentsCurrentStateComponent } from './components-current-state/components-current-state.component';
import { ComponentsHistoryComponent } from './components-history/components-history.component';
import { ComponentsProblemComponent } from './components-problem/components-problem.component';
import { ComponentsTypesComponent } from './components-types/components-types.component';
import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsLayoutComponent } from './components-layout/components-layout.component';

@NgModule({
  declarations: [
    ComponentsTree,
    ComponentsListComponent,
    ComponentsCurrentStateComponent,
    ComponentsHistoryComponent,
    ComponentsProblemComponent,
    ComponentsTypesComponent,
    ComponentsLayoutComponent,
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
  ],
  providers: []
})
export class ComponentsModule { }
