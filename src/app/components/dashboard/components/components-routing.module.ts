import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsTree } from './components-tree/components-tree.component';
import { ComponentsListComponent } from './components-list/components-list.component';
import { ComponentsCurrentStateComponent } from './components-current-state/components-current-state.component';
import { ComponentsHistoryComponent } from './components-history/components-history.component';
import { ComponentsProblemComponent } from './components-problem/components-problem.component';
import { ComponentsTypesComponent } from './components-types/components-types.component';
import { ComponentsLayoutComponent } from './components-layout/components-layout.component';

const routes: Routes = [
  {
    path: '',
    component:  ComponentsLayoutComponent,
    children: [
      {
        path: 'component-tree',
        component: ComponentsTree,
      },
      {
        path: 'list',
        component: ComponentsListComponent,
      },
      {
        path: 'current-state',
        component: ComponentsCurrentStateComponent,
      },
      {
        path: 'history',
        component: ComponentsHistoryComponent,
      },
      {
        path: 'problem-components',
        component: ComponentsProblemComponent,
      },
      {
        path: 'types',
        component: ComponentsTypesComponent,
      },
      {
        path: '',
        redirectTo: 'component-tree',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
