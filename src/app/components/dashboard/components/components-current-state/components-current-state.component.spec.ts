import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsCurrentStateComponent } from './components-current-state.component';

describe('ComponentsCurrentStateComponent', () => {
  let component: ComponentsCurrentStateComponent;
  let fixture: ComponentFixture<ComponentsCurrentStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsCurrentStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsCurrentStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
