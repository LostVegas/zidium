import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsEventTypesComponent } from './errors-event-types.component';

describe('ErrorsEventTypesComponent', () => {
  let component: ErrorsEventTypesComponent;
  let fixture: ComponentFixture<ErrorsEventTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorsEventTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsEventTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
