import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-errors-layout',
  template: `<router-outlet></router-outlet>`,
})
export class ErrorsLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
