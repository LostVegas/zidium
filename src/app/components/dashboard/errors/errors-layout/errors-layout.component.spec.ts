import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsLayoutComponent } from './errors-layout.component';

describe('ErrorsLayoutComponent', () => {
  let component: ErrorsLayoutComponent;
  let fixture: ComponentFixture<ErrorsLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorsLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
