import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ErrorsLayoutComponent} from './errors-layout/errors-layout.component';
import {ErrorsEventsComponent} from './errors-events/errors-events.component';
import {ErrorsEventTypesComponent} from './errors-event-types/errors-event-types.component';
import {ErrorsComponent} from './errors/errors.component';



const routes: Routes = [
  {
    path: '',
    component:  ErrorsLayoutComponent,
    children: [
      {
        path: 'errors',
        component: ErrorsComponent,
      },
      {
        path: 'events',
        component: ErrorsEventsComponent,
      },
      {
        path: 'event-types',
        component: ErrorsEventTypesComponent,
      },
      {
        path: '',
        redirectTo: 'errors',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorsRoutingModule { }
