import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorsEventsComponent } from './errors-events.component';

describe('ErrorsEventsComponent', () => {
  let component: ErrorsEventsComponent;
  let fixture: ComponentFixture<ErrorsEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorsEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorsEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
