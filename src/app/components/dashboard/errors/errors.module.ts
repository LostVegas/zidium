import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ErrorsLayoutComponent} from './errors-layout/errors-layout.component';
import {ErrorsEventTypesComponent} from './errors-event-types/errors-event-types.component';
import {ErrorsEventsComponent} from './errors-events/errors-events.component';
import {ErrorsRoutingModule} from './errors-routing.module';
import { ErrorsComponent } from './errors/errors.component';

@NgModule({
  declarations: [
    ErrorsEventTypesComponent,
    ErrorsEventsComponent,
    ErrorsLayoutComponent,
    ErrorsComponent,
  ],
  imports: [
    CommonModule,
    ErrorsRoutingModule,
  ],
  providers: []
})
export class ErrorsModule { }
