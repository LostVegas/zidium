import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { ClickOutsideModule } from 'ng-click-outside';
import { FooterComponent } from './components/shared/footer/footer.component';
import { SampleComponent } from './components/sample/sample.component';
import { ComponentsModule } from './components/dashboard/components/components.module';
import { StartComponent } from './components/dashboard/start/start.component';
import { ErrorsModule } from './components/dashboard/errors/errors.module';
import { MetricsModule } from './components/dashboard/metrics/metrics.module';
import { ChecksModule } from './components/dashboard/checks/checks.module';
import { LogsComponent } from './components/dashboard/logs/logs.component';
import { DefectsComponent } from './components/dashboard/defects/defects.component';
import { SettingsModule } from './components/dashboard/settings/settings.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SampleComponent,
    FooterComponent,
    StartComponent,
    LogsComponent,
    DefectsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClickOutsideModule,
    ComponentsModule,
    ErrorsModule,
    MetricsModule,
    ChecksModule,
    SettingsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
